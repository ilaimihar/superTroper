package com.example.al.supertroper;

import android.app.Application;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;


/**
 * Created by al on 11/13/15.
 */

//togirstreseseentitylight
//    105c2431129db8de9132cfbf0bf6b6774a93b58f
//    palseco , acra-lamp


//formUri = "http://pulsecostory.cloudant.com/acra-story/_design/acra-storage/_update/report",
//        reportType = HttpSender.Type.JSON,
//        httpMethod = HttpSender.Method.POST,
//        socketTimeout = 20000,
//        connectionTimeout = 20000,
//        formUriBasicAuthLogin = "heardsubbledgmerentontal",
//        formUriBasicAuthPassword = "79c86e219790270c05f24ada021f9201bcf43ecc",
@ReportsCrashes(

        formUri = "https://thawing-mesa-9618.herokuapp.com/reports",
        //formKey = "", // This is required for backward compatibility but not used
        customReportContent = {
                ReportField.LOGCAT ,
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast
)

public class MyApplication extends Application {

    private Tracker mTracker;

    private static final String PROPERTY_ID = "UA-61508305-3";


    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);

        int glideCacheSize = 20 * 1024 * 1024;
        GlideBuilder glideBuilder = new GlideBuilder(this);
        glideBuilder.setDiskCache(
                new InternalCacheDiskCacheFactory(this, glideCacheSize));

    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);

        }
        return mTracker;


    }

}
