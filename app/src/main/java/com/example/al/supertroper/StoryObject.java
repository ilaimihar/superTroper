package com.example.al.supertroper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by al on 11/14/15.
 */
public class StoryObject {

    public String url , blurredUrl , title , smallDetail , time , fullDetail , mp4Url , backgroundLoadingColor;
    public StoryObject(JSONObject jsonObject){
        init(jsonObject);
    }
    public StoryObject(JSONObject jsonObject, String blurredUrl){
        init(jsonObject);
        this.blurredUrl = "http://" + blurredUrl;
    }

    public StoryObject(String s){
        String [] arr = s.split("XXdivXX");
        time = arr[0];
        smallDetail = arr[1];
        url = arr[2];
        fullDetail = arr[3];
        title = arr[4];
        blurredUrl = arr[5];
        mp4Url = arr[6];
        backgroundLoadingColor = arr[7];
    }
    private  void init(JSONObject jsonObject){
        blurredUrl = "nully";

        try {
            url = "http://" + jsonObject.getString("url");
//            TODO check dat line :P
            mp4Url = "http://"+ jsonObject.getString("videoUrl");
//            backgroundLoadingColor = jsonObject.getString("background");
            title =jsonObject.getString("titleOfStory");
            smallDetail = jsonObject.getString("smallDetailOfStory");
            time = jsonObject.getString("time");
            fullDetail = jsonObject.getString("fullDetailOfStory");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String toString(){
        return time + "XXdivXX" + smallDetail +
                "XXdivXX" + url + "XXdivXX" + fullDetail + "XXdivXX"
                + title + "XXdivXX" + blurredUrl + "XXdivXX" + mp4Url
                + "XXdivXX" + backgroundLoadingColor
                ;
    }



}
